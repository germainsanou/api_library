<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];


    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (ValidationException $e, $request) {
            return response(['errors' => $e->errors()]);
        });

        $this->renderable(function (AccessDeniedHttpException $e, $request) {
            return response(['message' => 'This action is unauthorized'], 403);
        });

        $this->renderable(function (ModelNotFoundException $e, $request) {
            return response(['message' => 'Not found'], 404);
        });

        $this->renderable(function (NotFoundHttpException $e, $request) {
            return response(['message' => 'Something turn wrong'], 404);
        });

        $this->renderable(function (AuthenticationException $e, $request) {
            return response(['message' => 'Authentication error'], 200);
        });

        $this->renderable(function (Exception $e, $request) {
            return response($e);
        });
    }

}
