<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:2',
            'last_name' => 'required|string|min:2',
            'nationality' => 'nullable|string|min:6',
            'district' => 'nullable|string|min:3',
            'address' => 'nullable|string|min:6',
            'phone_number' => 'nullable|string|min:8',
            'occupation' => 'nullable|string|min:3',
            'birth_date' => 'nullable|date|before:today',
            'image' => 'nullable|image|max:512',
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ];
    }
}
