<?php

namespace App\Http\Requests\Library;

use Illuminate\Foundation\Http\FormRequest;

class LibraryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:6',
            'email' => 'required|email',
            'address' => 'required|string',
            'schedule' => 'nullable|string',

        ];
    }

    /**
     * 
     */
    public function bodyParameters()
    {
        return [
            'name' => [
                'description' => 'The name of the library.',
                'example' => 'Ouest book'
            ],

            'address' => [
                'description' => 'The address of the library.',
                'example' => 'Ouagadougou, rue Kalembanga'
            ],

            'schedule' => [
                'description' => 'The library timetables.',
                'example' => '24H sur 24 - 7J sur 7'
            ]
        ];
    }
}
