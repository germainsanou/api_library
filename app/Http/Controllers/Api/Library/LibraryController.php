<?php

namespace App\Http\Controllers\Api\Library;

use App\Http\Controllers\Controller;
use App\Models\Library;
use Illuminate\Http\Request;
use App\Http\Requests\Library\LibraryRequest;

/**
 * @group Library management
 * 
 * Endpoints for libraries management
 */
class LibraryController extends Controller
{
    /**
     * Index
     *
     * Display the list of libraies
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store
     *
     * Create new library
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LibraryRequest $request)
    {
        //
    }

    /**
     * Show
     *
     * Get a specifique library
     * @param  \App\Models\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function show($library)
    {
        //
    }

    /**
     * Update
     *
     * Update a specifique library
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function update(LibraryRequest $request, $library)
    {
        //
    }

    /**
     * Remove
     *
     * Delete a specifique library
     * @param  \App\Models\Library  $library
     * @return \Illuminate\Http\Response
     */
    public function destroy($library)
    {
        //
    }

}
