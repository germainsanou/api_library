<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
// use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use App\Models\User;

class VerificationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only('resend');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }


    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verify(Request $request)
    {
        $id = $request->route('id');
        $user = User::findOrFail($id);

        if (! $request->route('hash') == sha1($user->getEmailForVerification())) {
            throw new AuthorizationException;
        }


        if ($user->hasVerifiedEmail()) {
            // return redirect('http://google.fr');
            return response(['message' => 'Already verified']);
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }
        
        // return redirect('http://google.fr');
        return response(['message' => 'Successfuly verified']);
        
    }


    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response([
                'success' => true,
                'message' => 'Already verified'
            ]);
        }

        $request->user()->sendEmailVerificationNotification();

        return response([
            'success' => true,
            'message' => 'Email Sent'
        ]);
    }
}
