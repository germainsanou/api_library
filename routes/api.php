<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Auth\VerificationController;
use App\Http\Controllers\Api\Library\LibraryController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// AUTHENTICATION
Route::post('auth/register', [AuthController::class, 'register'])->name('register');
Route::post('auth/login', [AuthController::class, 'login'])->name('login');
Route::post('auth/logout', [AuthController::class, 'logout'])->name('logout');
Route::post('auth/refresh', [AuthController::class, 'refresh'])->name('refresh');
Route::post('auth/user', [AuthController::class, 'user'])->name('user');

Route::get('/email/resend', [VerificationController::class, 'resend'])
	->name('verification.resend');

Route::get('/email/verify/{id}/{hash}', [VerificationController::class, 'verify'])
	->name('verification.verify');

Route::post('password/email', [App\Http\Controllers\Api\Auth\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');

Route::post('password/reset', [App\Http\Controllers\Api\Auth\ResetPasswordController::class, 'reset'])->name('password.update');

Route::apiResource('/libraries', LibraryController::class)->parameters(['libraries' => 'library_id']);