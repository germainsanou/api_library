# Library management

Endpoints for libraries management

## Index


Display the list of libraies

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/libraries" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/libraries"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


<div id="execution-results-GETapi-libraries" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-libraries"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-libraries"></code></pre>
</div>
<div id="execution-error-GETapi-libraries" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-libraries"></code></pre>
</div>
<form id="form-GETapi-libraries" data-method="GET" data-path="api/libraries" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-libraries', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-libraries" onclick="tryItOut('GETapi-libraries');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-libraries" onclick="cancelTryOut('GETapi-libraries');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-libraries" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/libraries</code></b>
</p>
</form>


## Store


Create new library

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/libraries" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Ouest book","email":"twuckert@example.org","address":"Ouagadougou, rue Kalembanga","schedule":"24H sur 24 - 7J sur 7"}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/libraries"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Ouest book",
    "email": "twuckert@example.org",
    "address": "Ouagadougou, rue Kalembanga",
    "schedule": "24H sur 24 - 7J sur 7"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-libraries" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-libraries"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-libraries"></code></pre>
</div>
<div id="execution-error-POSTapi-libraries" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-libraries"></code></pre>
</div>
<form id="form-POSTapi-libraries" data-method="POST" data-path="api/libraries" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-libraries', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-libraries" onclick="tryItOut('POSTapi-libraries');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-libraries" onclick="cancelTryOut('POSTapi-libraries');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-libraries" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/libraries</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-libraries" data-component="body" required  hidden>
<br>
The name of the library.</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-libraries" data-component="body" required  hidden>
<br>
The value must be a valid email address.</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="address" data-endpoint="POSTapi-libraries" data-component="body" required  hidden>
<br>
The address of the library.</p>
<p>
<b><code>schedule</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="schedule" data-endpoint="POSTapi-libraries" data-component="body"  hidden>
<br>
The library timetables.</p>

</form>


## Show


Get a specifique library

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/libraries/{library_id}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/libraries/{library_id}"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


<div id="execution-results-GETapi-libraries--library_id-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-libraries--library_id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-libraries--library_id-"></code></pre>
</div>
<div id="execution-error-GETapi-libraries--library_id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-libraries--library_id-"></code></pre>
</div>
<form id="form-GETapi-libraries--library_id-" data-method="GET" data-path="api/libraries/{library_id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-libraries--library_id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-libraries--library_id-" onclick="tryItOut('GETapi-libraries--library_id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-libraries--library_id-" onclick="cancelTryOut('GETapi-libraries--library_id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-libraries--library_id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/libraries/{library_id}</code></b>
</p>
</form>


## Update


Update a specifique library

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/libraries/{library_id}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Ouest book","email":"rohan.karen@example.com","address":"Ouagadougou, rue Kalembanga","schedule":"24H sur 24 - 7J sur 7"}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/libraries/{library_id}"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Ouest book",
    "email": "rohan.karen@example.com",
    "address": "Ouagadougou, rue Kalembanga",
    "schedule": "24H sur 24 - 7J sur 7"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-PUTapi-libraries--library_id-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-libraries--library_id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-libraries--library_id-"></code></pre>
</div>
<div id="execution-error-PUTapi-libraries--library_id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-libraries--library_id-"></code></pre>
</div>
<form id="form-PUTapi-libraries--library_id-" data-method="PUT" data-path="api/libraries/{library_id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-libraries--library_id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-libraries--library_id-" onclick="tryItOut('PUTapi-libraries--library_id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-libraries--library_id-" onclick="cancelTryOut('PUTapi-libraries--library_id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-libraries--library_id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/libraries/{library_id}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/libraries/{library_id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="PUTapi-libraries--library_id-" data-component="body" required  hidden>
<br>
The name of the library.</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="PUTapi-libraries--library_id-" data-component="body" required  hidden>
<br>
The value must be a valid email address.</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="address" data-endpoint="PUTapi-libraries--library_id-" data-component="body" required  hidden>
<br>
The address of the library.</p>
<p>
<b><code>schedule</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="schedule" data-endpoint="PUTapi-libraries--library_id-" data-component="body"  hidden>
<br>
The library timetables.</p>

</form>


## Remove


Delete a specifique library

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/libraries/{library_id}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/libraries/{library_id}"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEapi-libraries--library_id-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-libraries--library_id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-libraries--library_id-"></code></pre>
</div>
<div id="execution-error-DELETEapi-libraries--library_id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-libraries--library_id-"></code></pre>
</div>
<form id="form-DELETEapi-libraries--library_id-" data-method="DELETE" data-path="api/libraries/{library_id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-libraries--library_id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-libraries--library_id-" onclick="tryItOut('DELETEapi-libraries--library_id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-libraries--library_id-" onclick="cancelTryOut('DELETEapi-libraries--library_id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-libraries--library_id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/libraries/{library_id}</code></b>
</p>
</form>



